/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

define([
    'Magento_Ui/js/form/components/button',
    'uiRegistry',
    'uiLayout',
    'mageUtils',
    'underscore'
], function (Element, registry, layout, utils, _) {
    'use strict';

    return Element.extend({
        /**
         * Performs configured actions
         */
        action: function () {
            if(this.redirectUrl){
                location.href = this.redirectUrl;
                return;
            }
            this.actions.forEach(this.applyAction, this);
        },

        /**
         * Apply action on target component,
         * but previously create this component from template if it is not existed
         *
         * @param {Object} action - action configuration
         */
        applyAction: function (action) {
            var targetName = action.targetName,
                params = action.params || [],
                extendParams = [],
                actionName = action.actionName,
                target;

            if(action.fields){
                target = registry.get(targetName);
                params = target.externalSource().params;
                action.fields.forEach(function(el){
                    if (!registry.has(el)) {
                        this.getFromTemplate(el);
                    }
                    var field = registry.get(el);
                    params[field.index] = field.value();
                });
                if (!registry.has(targetName)) {
                    this.getFromTemplate(targetName);
                }
                target.externalSource().set('params', params);
                target.reload();
                return;
            }

            if (!registry.has(targetName)) {
                this.getFromTemplate(targetName);
            }
            target = registry.async(targetName);

            if (target && typeof target === 'function' && actionName) {
                params.unshift(actionName);
                target.apply(target, params);
            }
        },
    });
});
