<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Model\ResourceModel\Staff\Staff\Grid;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Ui\Model\Export\MetadataProvider;
use Magestore\Webpos\Model\Source\Adminhtml\Location;
use Psr\Log\LoggerInterface as Logger;

/**
 * Staff grid Collection
 */
class Collection extends SearchResult
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    /**
     * @var Location
     */
    protected $location;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Collection constructor.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     * @param Location $location
     * @param RequestInterface $request
     * @throws LocalizedException
     */
    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        $mainTable,
        $resourceModel,
        Location $location,
        RequestInterface $request
    ) {
        $this->location = $location;
        $this->request = $request;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    /**
     * Get data
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getData()
    {
        $data = parent::getData();
        if (($this->request->getActionName() == 'gridToCsv') || ($this->request->getActionName() == 'gridToXml')) {
            $options = [
                self::STATUS_ENABLED => __('Enabled'),
                self::STATUS_DISABLED => __('Disabled')
            ];
            $locationOptions = $this->location->getOptionArray();
            foreach ($data as &$item) {
                if ($item['location_ids']) {
                    $locationArray = explode(',', $item['location_ids']);
                    $locationNameArray = [];
                    foreach ($locationArray as $locationId) {
                        if (isset($locationOptions[$locationId])) {
                            $locationName = $locationOptions[$locationId];
                            $locationNameArray[] = $locationName;
                        }
                    }
                    $item['location_ids'] = implode(',', $locationNameArray);
                }
                $objectManager = ObjectManager::getInstance();
                $metadataProvider = $objectManager->get(MetadataProvider::class);
                if (!method_exists($metadataProvider, 'getColumnOptions')) {
                    if ($item['status']) {
                        $item['status'] = $options[$item['status']];
                    }
                }
            }
        }
        return $data;
    }
}
