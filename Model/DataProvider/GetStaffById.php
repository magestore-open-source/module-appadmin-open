<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

declare(strict_types = 1);

namespace Magestore\Appadmin\Model\DataProvider;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Data\Staff\StaffInterface;
use Magestore\Appadmin\Api\DataProvider\GetStaffByIdInterface;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;

/**
 * Class GetCurrentStaffById
 *
 * Data Provider to get current staff by id
 */
class GetStaffById extends DataObject implements GetStaffByIdInterface
{
    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * GetCurrentStaffById constructor.
     *
     * @param StaffRepositoryInterface $staffRepository
     * @param array $data
     */
    public function __construct(
        StaffRepositoryInterface $staffRepository,
        $data = []
    ) {
        $this->staffRepository = $staffRepository;
        parent::__construct($data);
    }

    /**
     * Get staff by id
     *
     * @param int $id
     * @return StaffInterface|null
     * @throws LocalizedException
     */
    public function execute(int $id)
    {
        $staffData = $this->_getData(self::STAFF_DATA);
        if (!is_array($staffData)) {
            $staffData = [];
        }
        if (!isset($staffData[$id])) {
            $staffData[$id] = $this->staffRepository->getById($id);
            $this->setData(self::STAFF_DATA, $staffData);
        }
        return $staffData[$id];
    }
}
