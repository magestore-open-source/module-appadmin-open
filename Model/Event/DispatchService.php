<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Model\Event;

use Magento\Framework\Event\ManagerInterface;
use Magestore\Appadmin\Api\Event\DispatchServiceInterface;

/**
 * Event Dispatch Service
 */
class DispatchService implements DispatchServiceInterface
{
    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * DispatchService constructor.
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        ManagerInterface $eventManager
    ) {
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritdoc
     */
    public function dispatchEventForceSignOut($staffId, $posId = null)
    {
        $this->eventManager->dispatch(self::EVENT_NAME_FORCE_SIGN_OUT, ['staff_id' => $staffId, 'pos_id' => $posId]);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function dispatchEventForceChangePos($staffId, $posId)
    {
        $this->eventManager->dispatch(self::EVENT_NAME_FORCE_CHANGE_POS, ['staff_id' => $staffId, 'pos_id' => $posId]);
        return true;
    }
}
