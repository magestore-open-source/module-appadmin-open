<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Api\Data\Staff;

/**
 * Interface StaffInterface
 */
interface StaffInterface
{
    const STAFF_ID = "staff_id";
    const USER_NAME = 'username';
    const PASSWORD = 'password';
    const NAME = 'name';
    const EMAIL = 'email';
    const CUSTOMER_GROUPS = 'customer_groups';
    const LOCATION_IDS = 'location_ids';
    const STATUS = 'status';
    const STATUS_ENABLED = 1;
    const MIN_PASSWORD_LENGTH = 7;
    const POS_IDS = 'pos_ids';
    const PIN = 'pin';

    /**
     * Get staff id
     *
     * @api
     * @return int
     */
    public function getStaffId();

    /**
     * Set staff id
     *
     * @api
     * @param int $staffId
     * @return $this
     */
    public function setStaffId($staffId);
    /**
     * Get user name
     *
     * @api
     * @return string|null
     */
    public function getUsername();

    /**
     * Set user name
     *
     * @api
     * @param string $username
     * @return $this
     */
    public function setUsername($username);
    /**
     * Get password param
     *
     * @api
     * @return string|null
     */
    public function getPassword();

    /**
     * Set password param
     *
     * @api
     * @param string $password
     * @return $this
     */
    public function setPassword($password);
    /**
     * Get display name
     *
     * @api
     * @return string|null
     */
    public function getName();

    /**
     * Set display name
     *
     * @api
     * @param string $name
     * @return $this
     */
    public function setName($name);
    /**
     * Get email
     *
     * @api
     * @return string|null
     */
    public function getEmail();

    /**
     * Set display name
     *
     * @api
     * @param string $email
     * @return $this
     */
    public function setEmail($email);
    /**
     * Get customer group
     *
     * @api
     * @return string|null
     */
    public function getCustomerGroups();

    /**
     * Set customer group
     *
     * @api
     * @param string $customerGroups
     * @return $this
     */
    public function setCustomerGroups($customerGroups);
    /**
     * Get location id
     *
     * @api
     * @return string|null
     */
    public function getLocationIds();

    /**
     * Set location id
     *
     * @api
     * @param string $locationIds
     * @return $this
     */
    public function setLocationIds($locationIds);
    /**
     * Get status
     *
     * @api
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @api
     * @param string $status
     * @return $this
     */
    public function setStatus($status);
    /**
     * Get pos id
     *
     * @api
     * @return string|null
     */
    public function getPosIds();

    /**
     * Set pos id
     *
     * @api
     * @param string $posIds
     * @return $this
     */
    public function setPosIds($posIds);
}
