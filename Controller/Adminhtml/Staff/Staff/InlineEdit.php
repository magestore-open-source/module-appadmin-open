<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace  Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magestore\Appadmin\Api\Data\Staff\StaffInterfaceFactory;
use Psr\Log\LoggerInterface;

/**
 * Inline edit user
 */
class InlineEdit extends Action implements HttpPostActionInterface
{

    /** @var JsonFactory  */
    protected $resultJsonFactory;

    /** @var DataObjectHelper  */
    protected $dataObjectHelper;

    /** @var LoggerInterface */
    protected $logger;
    /**
     * @var StaffInterfaceFactory
     */
    protected $staffInterfaceFactory;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param StaffInterfaceFactory $staffInterfaceFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        DataObjectHelper $dataObjectHelper,
        StaffInterfaceFactory $staffInterfaceFactory,
        LoggerInterface $logger
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->logger = $logger;
        $this->staffInterfaceFactory = $staffInterfaceFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return Json
     */
    public function execute()
    {

        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $model = $this->staffInterfaceFactory->create();
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $staffId) {
            $model->setData($postItems[$staffId]);
            $model->setId($staffId);
            $model->save();
        }

        return $resultJson->setData([
            'messages' => $this->getErrorMessages(),
            'error' => $this->isErrorExists(),

        ]);
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magestore_Appadmin::manageStaffs');
    }

    /**
     * Get Error Messages
     *
     * @return array
     */
    protected function getErrorMessages()
    {
        $messages = [];
        foreach ($this->getMessageManager()->getMessages()->getItems() as $error) {
            $messages[] = $error->getText();
        }
        return $messages;
    }

    /**
     * Is Error Exists
     *
     * @return bool
     */
    protected function isErrorExists()
    {
        return (bool)$this->getMessageManager()->getMessages(true)->getCount();
    }
}
