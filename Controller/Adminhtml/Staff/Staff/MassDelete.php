<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magestore\Appadmin\Api\Staff\StaffRepositoryInterface;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magestore\Appadmin\Model\ResourceModel\Staff\Staff\Collection;

/**
 * Mass delete staff
 */
class MassDelete extends AbstractMassAction implements HttpPostActionInterface
{
    /**
     * @var StaffRepositoryInterface
     */
    protected $staffRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param StaffRepositoryInterface $staffRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        StaffRepositoryInterface $staffRepository
    ) {
        $this->staffRepository = $staffRepository;
        parent::__construct($context, $filter, $collectionFactory);
    }

    /**
     * Mass Action
     *
     * @param Collection $collection
     * @return Redirect|mixed
     * @throws LocalizedException
     */
    protected function massAction(Collection $collection)
    {
        $staffDeleted = 0;
        foreach ($collection as $staff) {
            $this->staffRepository->delete($staff);
            $staffDeleted++;
        }

        if ($staffDeleted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were deleted.', $staffDeleted));
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}
