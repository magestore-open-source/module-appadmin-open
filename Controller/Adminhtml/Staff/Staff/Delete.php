<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
namespace Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

use Exception;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magestore\Appadmin\Controller\Adminhtml\Staff\Staff;

/**
 * Delete user
 */
class Delete extends Staff implements HttpPostActionInterface
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $userId = $this->getRequest()->getParam('id');
        if ($userId > 0) {
            try {
                $this->staffRepository->deleteById($this->getRequest()->getParam('id'));
                $this->messageManager->addSuccessMessage(__('Staff was successfully deleted'));
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['_current' => true]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
