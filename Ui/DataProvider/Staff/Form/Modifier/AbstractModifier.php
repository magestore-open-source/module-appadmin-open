<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Ui\DataProvider\Staff\Form\Modifier;

use Magestore\Appadmin\Model\Staff\Staff;

/**
 * Staff form - AbstractModifier
 */
class AbstractModifier extends \Magestore\Appadmin\Ui\DataProvider\Form\Modifier\AbstractModifier
{
    /**
     * @var Staff
     */
    protected $currentStaff;

    /**
     * @var string
     */
    protected $scopeName = 'appadmin_staff_form.appadmin_staff_form';

    /**
     * GetCurrentStaff
     *
     * @return Staff|mixed
     */
    public function getCurrentStaff()
    {
        if (!$this->currentStaff) {
            $this->currentStaff = $this->registry->registry('current_staff');
        }
        return $this->currentStaff;
    }
}
