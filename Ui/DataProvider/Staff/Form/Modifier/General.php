<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Appadmin\Ui\DataProvider\Staff\Form\Modifier;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Ui\Component\Listing\Column\Store\Options;
use Magento\Ui\Component\Form;
use Magento\Ui\Component\Form\Field;
use Magestore\Appadmin\Model\Source\Adminhtml\Status;
use Magestore\Webpos\Model\Source\Adminhtml\CustomerGroup;
use Magestore\Webpos\Model\Source\Adminhtml\Location;
use Magestore\Webpos\Model\Source\Adminhtml\Pos;

/**
 * Staff form - General modifier
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class General extends AbstractModifier
{
    /**
     * @var Options
     */
    protected $optionStoreView;

    /**
     * @var string
     */
    protected $groupContainer = 'general_information';

    /**
     * @var string
     */
    protected $groupLabel = 'General Information';

    /**
     * @var int
     */
    protected $sortOrder = 80;

    /**
     * @var bool
     */
    protected $opened = true;
    /**
     * @var CustomerGroup
     */
    protected $customerGroup;
    /**
     * @var Status
     */
    protected $status;
    /**
     * @var Location
     */
    protected $location;
    /**
     * @var Pos
     */
    protected $pos;
    
    /**
     * General constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param Registry $registry
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param Options $optionStoreView
     * @param CustomerGroup $customerGroup
     * @param Status $status
     * @param Location $location
     * @param Pos $pos
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Registry $registry,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        Options $optionStoreView,
        CustomerGroup $customerGroup,
        Status $status,
        Location $location,
        Pos $pos
    ) {
        parent::__construct($objectManager, $registry, $request, $urlBuilder);
        $this->optionStoreView = $optionStoreView;
        $this->customerGroup = $customerGroup;
        $this->status = $status;
        $this->location = $location;
        $this->pos = $pos;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                'staff_information' => [
                    'children' => $this->getGeneralChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Staff Information'),
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => 1
                            ],
                        ],
                    ],
                ],
                'user_settings' => [
                    'children' => $this->getUserSettings(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('POS permission'),
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => $this->getVisible(),
                                'opened' => $this->getOpened(),
                                'componentType' => Form\Fieldset::NAME,
                                'sortOrder' => 2
                            ],
                        ],
                    ],
                ],

            ]
        );
        return $meta;
    }

    /**
     * Get General Children
     *
     * @return array
     */
    protected function getGeneralChildren()
    {
        $children = [
            'username' => $this->addFormFieldText('User Name', 'input', 10, ['required-entry' => true]),
            'password' => $this->addFormFieldPassword(
                __('Password'),
                'input',
                20,
                [
                    'validate-admin-password' => true
                ],
                'input-text required-entry validate-admin-password'
            ),
            'password_confirmation' => $this->addFormFieldPassword(
                __('Password Confirmation'),
                'input',
                30,
                [
                    'equalTo' => '#password'
                ],
                'input-text required-entry validate-cpassword'
            ),
            'name' => $this->addFormFieldText('Display Name', 'input', 40, ['required-entry' => true]),
            'email' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'label' => __('Email Address'),
                            'dataType' => 'text',
                            'formElement' => 'input',
                            'sortOrder' => 50,
                            'validation' => [
                                'required-entry' => true,
                                'validate-email' => true
                            ],
                        ],
                    ],
                ],
            ],
            'status' => $this->addFormFieldSelect(
                'Status',
                70,
                $this->status->toOptionArray(),
                ['required-entry' => true]
            ),
        ];

        if (!$this->request->getParam('id')) {
            $children['password']['arguments']['data']['config']['validation']['required-entry'] = true;
            $children['password_confirmation']['arguments']['data']['config']['validation']['required-entry'] = true;
        } else {
            $children['password']['arguments']['data']['config']['label'] = __("New Password");
        }
        return $children;
    }

    /**
     * Get User Settings
     *
     * @return array
     */
    protected function getUserSettings()
    {
        $children = [
            'location_ids' => $this->addFormFieldMultiSelect(
                'Location',
                20,
                $this->location->toOptionArray(),
                ['required-entry' => true]
            )
        ];
        return $children;
    }

    /**
     * Retrieve countries
     *
     * @return array|null
     */
    protected function getStoreViews()
    {
        return $this->optionStoreView->toOptionArray();
    }
}
