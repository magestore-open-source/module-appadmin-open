<?php
/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */
declare(strict_types=1);

namespace Magestore\Appadmin\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magestore\Appadmin\Model\Staff\StaffFactory;
use Magestore\Webpos\Model\Location\Location;
use Magestore\Webpos\Model\ResourceModel\Location\Location\CollectionFactory as LocationCollectionFactory;
use Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

/**
 * Create first staff after installing
 */
class CreateFirstStaff implements DataPatchInterface
{
    const IS_ACTIVE = 1;
    const NOT_ENCODE_PASSWORD = 1;

    /**
     * @var StaffFactory
     */
    protected $_staffFactory;

    /**
     * @var UserCollectionFactory
     */
    protected $_userCollectionFactory;

    /**
     * @var LocationCollectionFactory
     */
    protected $_locationCollectionFactory;

    /**
     * @param UserCollectionFactory $userCollectionFactory
     * @param StaffFactory $staffFactory
     * @param LocationCollectionFactory $locationCollectionFactory
     */
    public function __construct(
        UserCollectionFactory $userCollectionFactory,
        StaffFactory $staffFactory,
        LocationCollectionFactory $locationCollectionFactory
    ) {
        $this->_userCollectionFactory = $userCollectionFactory;
        $this->_staffFactory = $staffFactory;
        $this->_locationCollectionFactory = $locationCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /**
         * Setup Staff Data
         */
        $userModel = $this->_userCollectionFactory->create()
            ->addFieldToFilter('is_active', self::IS_ACTIVE)->getFirstItem();
        /** @var Location $locationModel */
        $locationModel = $this->_locationCollectionFactory->create()
            ->getFirstItem();
        $locationId = $locationModel->getId();
        if (!$locationId) {
            $locationId = 1;
        }
        if ($userModel->getId()) {
            $staffData = [
                'username' => $userModel->getUsername(),
                'password' => $userModel->getPassword(),
                'name' => $userModel->getFirstname() . ' ' . $userModel->getLastname(),
                'email' => $userModel->getEmail(),
                'location_ids' => $locationId,
                'not_encode' => self::NOT_ENCODE_PASSWORD,
                'status' => self::IS_ACTIVE
            ];
            $this->_staffFactory->create()->setData($staffData)->save();
        }
        return $this;
    }
}
